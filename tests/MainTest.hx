package tests;

import utest.Runner;
import utest.ui.Report;

class MainTest {
  public static function main() {
    //the long way
    var runner = new Runner();
    runner.addCase(new TestCaseSimple());
    Report.create(runner);
    runner.run();

    //the short way in case you don't need to handle any specifics
    //utest.UTest.run([new TestCase1(), new TestCase2()]);
  }
}
