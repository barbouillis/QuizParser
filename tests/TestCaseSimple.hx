package tests;

import utest.Assert;
import utest.Async;
import QuizBank;

class TestCaseSimple extends utest.Test {


	function specsTags(){

	var questions ="
// [tag:basic] [tag:test]
::TrueStatement about Grant::Grant was buried in a tomb in New York City.{T}

// question: 0 name: FalseStatement using {FALSE} style
::FalseStatement about sun::The sun rises in the West.{FALSE}
";


	var qb = new QuizBank().loadBankFromString(questions);
	qb.getQuestionWithId(0).tags[0] == "basic";
	qb.getQuestionWithId(0).tags[1] == "test";
}


	function specsIds(){

	var questions ="


::Yaya::Grant rere was buried in a tomb in New York City.{T}

// [id:925] [tag:test]
::TrueStatement about Grant::Grant was buried in a tomb in New York City.{T}

// [id:0]
::FalseStatement about sun::The sun rises in the West.{FALSE}
";


	var qb = new QuizBank().loadBankFromString(questions);
	qb.getQuestionWithId(0).title == "FalseStatement about sun";
	qb.getQuestionWithId(925).title == "TrueStatement about Grant";
	qb.getQuestionWithId(1).title == "Yaya";
}


	function specsSpecialCharacters(){
	var  questions= "
::title\\#:: Which answer equals 5? {
   ~ \\= 2 + 2
   = \\= 2 + 3
   ~ \\= 2 + 4
}
";
	var qb = new QuizBank().loadBankFromString(questions);
	qb.getQuestionWithId(0).title == "title#";
}

	function specsTrueFalse(){
	
	var questions ="
// question: 0 name: TrueStatement using {T} style
::TrueStatement about Grant::Grant was buried in a tomb in New York City.{T}

// question: 0 name: FalseStatement using {FALSE} style
::FalseStatement about sun::The sun rises in the West.{FALSE}
";
	var qb = new QuizBank().loadBankFromString(questions);
	qb.questions.length == 2;
	qb.getQuestionWithId(0).type == Question.QuestionType.TrueFalse;
	qb.getQuestionWithId(0).title == "TrueStatement about Grant";
	qb.getQuestionWithId(0).text == "Grant was buried in a tomb in New York City.";
	qb.getQuestionWithId(1).type == Question.QuestionType.TrueFalse;
	qb.getQuestionWithId(1).title == "FalseStatement about sun";
	qb.getQuestionWithId(1).text == "The sun rises in the West.";


	//Assert.equals(mm.getValue().get("another2"), "# This is not a= comment");

}


	function specsMultichoice(){
		var multi = "
Who's buried in Grant's tomb?{=Grant ~no one ~Napoleon ~Churchill ~Mother Teresa}

question: 1 name: Grants tomb
::Grants tomb::Who is buried in Grant's tomb in New York City? {
=Grant
~No one
#Was true for 12 years, but Grant's remains were buried in the tomb in 1897
~Napoleon
#He was buried in France
~Churchill
#He was buried in England
~Mother Teresa
#She was buried in India
}
";
		var qb = new QuizBank().loadBankFromString(multi);
		qb.getQuestionWithId(0).nbrAnswers == 5;
		qb.getQuestionWithId(0).correctAnswer == "Grant";
		qb.getQuestionWithId(0).type == Question.QuestionType.Multichoice;
		qb.getQuestionWithId(1).nbrAnswers == 5;
		qb.getQuestionWithId(1).correctAnswer == "Grant";
		qb.getQuestionWithId(1).type == Question.QuestionType.Multichoice;
		qb.getQuestionWithId(1).answers.list[1].feedback == "Was true for 12 years, but Grant's remains were buried in the tomb in 1897";

	}
	@Ignored
	function specsMultichoiceWithMultipleAnswers(){
		var multi = "
What two people are entombed in Grant's tomb? {
   ~%-100%No one
   ~%50%Grant
   ~%50%Grant's wife
   ~%-100%Grant's father
}
";
		var qb = new QuizBank().loadBankFromString(multi);
		qb.getQuestionWithId(0).nbrAnswers == 4;
		qb.getQuestionWithId(0).type == Question.QuestionType.MultiAnswers;

	}
	@Ignored
	function specsShortAnswer(){
		var short = "
Who's buried in Grant's tomb?{=Grant =Ulysses S. Grant =Ulysses Grant}

Two plus two equals {=four =4}
";
		var qb = new QuizBank().loadBankFromString(short);
		qb.getQuestionWithId(0).type == Question.QuestionType.Short;
		qb.getQuestionWithId(0).nbrAnswers == 3;
		qb.getQuestionWithId(1).nbrAnswers == 2;
	}
@Ignored
	function specsMatching(){
		var matching =
"
Match the following countries with their corresponding capitals. {
   =Canada -> Ottawa
   =Italy  -> Rome
   =Japan  -> Tokyo
   =India  -> New Delhi
   }
";
	var qb = new QuizBank().loadBankFromString(matching);
	qb.getQuestionWithId(0).type == Question.QuestionType.Match;
	qb.getQuestionWithId(0).text == "Match the following countries with their corresponding capitals.";
	qb.getQuestionWithId(0).nbrAnswers == 4;
	qb.getQuestionWithId(0).isMissingWord() == false;

}

@Ignored
	function specsMissingWords(){
		var missingWords=
"
Moodle costs {~lots of money =nothing ~a small amount} to download from moodle.org.

Mahatma Gandhi's birthday is an Indian holiday on  {
~15th
~3rd
=2nd
} of October.

Since {
  ~495 AD
  =1066 AD
  ~1215 AD
  ~ 43 AD
}
the town of Hastings England has been \"famous with visitors\".
";
	var qb = new QuizBank().loadBankFromString(missingWords);
	trace(qb.getQuestionWithId(0).text+"_");
	trace(qb.getQuestionWithId(0).textAfter+"_");
	qb.getQuestionWithId(0).isMissingWord() == true;
	qb.getQuestionWithId(1).isMissingWord() == true;
	qb.getQuestionWithId(2).isMissingWord() == true;
		trace(qb);
}
	@Ignored
	function specsNumericalQuestions(){
		var missingWords=
"
When was Ulysses S. Grant born?{#1822:5}
";
		var qb = new QuizBank().loadBankFromString(missingWords);
		trace(qb);
}
	@Ignored
	function specsEssay(){
		var essay=
"
Write a short biography of Dag Hammarskjöld. {}
";
		var qb = new QuizBank().loadBankFromString(essay);
		trace(qb);
}

@Ignored
function specsDescription(){
		var description=
"
You can use your pencil and paper for these next math questions.
";
		var qb = new QuizBank().loadBankFromString(description);
		trace(qb);
}

@Ignored
function specsFeedback(){
	var feedbacks = 
"
What's the answer to this multiple-choice question? {
  ~wrong answer#feedback comment on the wrong answer
  ~another wrong answer#feedback comment on this wrong answer
  =right answer#Very good!
}
 
//From The Hitchhiker's Guide to the Galaxy
Deep Thought said \" {
  =forty two#Correct according to The Hitchhiker's Guide to the Galaxy!
  =42#Correct, as told to Loonquawl and Phouchg
  =forty-two#Correct!
}  is the Ultimate Answer to the Ultimate Question of Life, The Universe, and Everything.\"

   42 is the Absolute Answer to everything.{
FALSE#42is the Ultimate Answer.#You gave the right answer.}
";
var qb = new QuizBank().loadBankFromString(feedbacks);
		trace(qb);
}

@Ignored
function specsPercentageWeights(){
	var feedbacks = 
"
::Jesus' hometown::Jesus Christ was from {
   ~Jerusalem#This was an important city, but the wrong answer.
   ~%25%Bethlehem#He was born here, but not raised here.
   ~%50%Galilee#You need to be more specific.
   =Nazareth#Yes! That's right!
}.
    
::Jesus' hometown:: Jesus Christ was from {
   =Nazareth#Yes! That's right!
   =%75%Nazereth#Right, but misspelled.
   =%25%Bethlehem#He was born here, but not raised here.
}
";
var qb = new QuizBank().loadBankFromString(feedbacks);
		trace(qb);
}


function specsRealWorld(){
	var questions ="

Qu'est-ce qui a été découvert accidentellement ? {
	~%50% Le verre laminé utilisé dans les pares-brises  # Le verre feuilleté a été inventé par Édouard Bénédictus en 1903 à la suite d'un concours de circonstances qui veut qu'il fit tomber accidentellement un bécher qui avait été rempli d'une solution liquide de matière plastique, qui, au lieu de se briser à l'impact, se fendit
	~%50% Les effets du LSD  #  Le dr Hoffmann avait pour objectif de développer un stimulant circulatoire mais est rentré accidentellement en contact avec le produit
}
";
	var qb = new QuizBank().loadBankFromString(questions);
	trace(qb);




}


}
