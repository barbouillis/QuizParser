# Quiz GIFT Parser





What is GIFT ?

GIFT is an awesome format to describe quizzes, it was invented by Moodle. The create thing is that you can write it by hand .



https://docs.moodle.org/310/en/GIFT_format



```gift
// true/false
::Q1:: 1+1=2 {T}

// multiple choice with specified feedback for right and wrong answers
::Q2:: What's between orange and green in the spectrum? 
{ =yellow # right; good! ~red # wrong, it's yellow ~blue # wrong, it's yellow }

// fill-in-the-blank [id:123] [tag:basic] [tag:math]
::Q3:: Two plus {=two =2} equals four.

// matching
::Q4:: Which animal eats which food? { =cat -> cat food =dog -> dog food }

// math range question
::Q5:: What is a number from 1 to 5? {#3:2}

// math range specified with interval end points
::Q6:: What is a number from 1 to 5? {#1..5}
// translated on import to the same as Q5, but unavailable from Moodle question interface

// multiple numeric answers with partial credit and feedback
::Q7:: When was Ulysses S. Grant born? {#
         =1822:0      # Correct! Full credit.
         =%50%1822:2  # He was born in 1822. Half credit for being close.
}

// essay
::Q8:: How are you? {}
```

## TODO

haxe

Multianswers without number

~%%  fills automatically







```
$CATEGORY: tom/dick/harry
```

```
$question->usecase = 0; // Ignore case
```

Stringify the questions

Make blocks



### Line comments

Implemented

```gift
//  [id:123] [tag:basic] [tag:set 1]
```


