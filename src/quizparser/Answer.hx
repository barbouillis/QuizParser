package quizparser;

import quizparser.Answers;

enum AnswerType {
	TrueFalse;
	Match;
	Multiple;
	MultipleOrShort;  // When it starts with "=", it can be a Multiple or a Short question
	Numerical;
}

class Answer {


		public var text : String;
		@:optional public var format: String = "text"; 
		@:optional public var fraction : Int;  
		public var feedback: String = ""; 
		@:optional public var answerType : AnswerType;

  public function new ():Void {
  }

  public function isValid():Bool {
	  return true;
  }


  public static function parseAnswer(s:String, asNumerical:Bool = false):Answer {

  		// Parsing Feedback
		var re = ~/[^\\\\]#/;  // We don't use backlooking because we need the # to be at the least the second character
		var endTextPos = s.length;
		var feedback = "";
		if (re.match(s)){
			feedback = Question.escape(StringTools.trim(re.matchedRight()));
			//s.substr(re.matchedPos().pos+re.matchedPos().len);
			endTextPos = re.matchedPos().pos+1;
			if (feedback == null) feedback ="";
		}

		var fC = s.charAt(0);


		// Parsing Fraction
		s = StringTools.trim(s.substr(0,endTextPos));
		var scoreReg = ~/%(.*)%/;
		var fraction = 0;
		var hasFraction = scoreReg.match(s);
		if (hasFraction){
			fraction = Std.parseInt(scoreReg.matched(1));
			s = fC + scoreReg.matchedRight(); // Why it doesn't work ?
		}

		

		var a:Answer = null;
		

		switch(fC){
			case "F" :
				var an = new Answer.AnswerTrueFalse();
				//a.text     = "false";
				an.result = false;
				an.parseMistakenFeedback(s);
				an.answerType = AnswerType.TrueFalse;
				a= an;
			case "T" :
				fraction = 100;
				var an = new Answer.AnswerTrueFalse();
				//a.text     = "true";
				an.result = true;
				an.parseMistakenFeedback(s);
				an.answerType = AnswerType.TrueFalse;
				a = an;
			case "=" :
				//var matchingReg = ~/(?<!\\)->/; // No negative  behind in old browsers
				var matchingReg = ~/[^\\]->/;
				//s = s.substr(1);

				if (matchingReg.match(s)){
					fraction = 100;
					a = new AnswerMatch();
					a.answerType = AnswerType.Match;
					a.text  = Question.escape(StringTools.trim(s.substr(1)));
					var am : AnswerMatch= cast a;
					am.parseMatch(a.text);
				}
			else if (asNumerical){
				if (fraction == 0)  fraction = 100;
					var an = new AnswerNumerical();
					an.answerType = AnswerType.Numerical;
					an.parseNumerical(Question.escape(StringTools.trim(s.substr(1))));
					a = an;
				}
			else {
				fraction = 100;
					a = new Answer();
					a.text  = Question.escape(StringTools.trim(s.substr(1)));
					a.answerType = AnswerType.MultipleOrShort;
				}
			case "~" :
				a = new Answer();
				//if (hasFraction) a.answerType = AnswerType.MultipleAnswers;
				//else {
					a.answerType = AnswerType.Multiple;
				//}
				a.text  = Question.escape(StringTools.trim(s.substr(1)));
			default :

				if (asNumerical){
					var an = new AnswerNumerical();
					an.answerType = AnswerType.Numerical;
					an.parseNumerical(Question.escape(StringTools.trim(s)));
					fraction = 100;
					a = an;

				}
				else {
					return null;
				}

				
		}

		a.fraction = fraction;
		a.feedback = feedback;

		return a;


  }


public function clone(){

	var a = new Answer();
	a.text = this.text;
	a.format = this.format;
	a.fraction = this.fraction;
	a.feedback = this.feedback;
	a.answerType = this.answerType;
	return a;
}

public function toGift(answers:Answers):String {
	return "" + (fraction == 100 ? "=" : "~" + (fraction != 0 ? "%"+fraction+"%" : "") ) +  Question.unescape(text) + (feedback != null && feedback!= "" ? "#" + Question.unescape(feedback) : "");
}


 public function toString():String {
	 return "answer of " +  this.fraction + "  "+ this.text + " in format " + this.format + " with feedback " + this.feedback;
 } 

}

class AnswerTrueFalse extends Answer{

	public var result:Bool;
	public var mistakenFeedback:String;

	public function parseMistakenFeedback(s){
		// Parsing Feedback
		var re = ~/[^\\\\]#/;  // We don't use backlooking because we need the # to be at the least the second character
		if (re.match(s)){
			mistakenFeedback = Question.escape(StringTools.trim(re.matchedRight()));
		}


	}

	override public function toGift(answers:Answers):String {
		var s = result ? "T" : "F";
		if (feedback != null && feedback != "") s += "#" + Question.unescape(feedback);
		return s;
	}


}

class AnswerNumerical extends Answer{

	public var value:Null<Float>;
	public var minimumValue:Float;
	public var maximumValue:Float;


	public function parseNumerical(s:String){
		var i = s.indexOf(":");
		if (i != -1){
			value = Std.parseFloat(s.substr(0,i));
			var range    = Std.parseFloat(s.substr(i+1));
			minimumValue = value - range;
			maximumValue = value + range;
			return;
		}
		var i = s.indexOf("..");
		if (i != -1){
			minimumValue = Std.parseFloat(s.substr(0,i));
			maximumValue    = Std.parseFloat(s.substr(i+2));
			return;
		}
		minimumValue = Std.parseFloat(s);
		maximumValue = minimumValue;
	}

	public override function clone(){
		var a:AnswerNumerical = cast super.clone();
		a.value = this.value;
		a.minimumValue = this.minimumValue;
		a.maximumValue = this.maximumValue;
		return a;
	}

	override public function toGift(answers:Answers){
		var s = ""+ minimumValue;
		if (maximumValue != minimumValue) s+=".."+maximumValue; 
		
		if (answers.list.length > 1) {
			
			if (fraction != 100) {
				s = "=%"+fraction + "%" + s;
			}
			else {
				s = "=" + s;
			}
		}

		/*
		if (answers.list[0] == this) {
			s = "#" + s;
		}*/

		s += (feedback!= "" ? "#" + Question.unescape(feedback) : "");
		
		
		return s;
	}
}


class AnswerMatch extends Answer{

	public var textRight:String;
	public var textLeft:String;


	public function parseMatch(s:String){

		//var matchingReg = ~/(?<!\\)->/;

		if  (StringTools.startsWith(s, "->")) {
			textLeft = "";
			textRight = s.substr(2);
			return;
		}

		var matchingReg = ~/[^\\]->/;
		matchingReg.match(s);
		textRight = matchingReg.matchedRight();
		textLeft  = s.substr(0, matchingReg.matchedPos().pos +1); 
	}

	public override function clone(){
		var a:AnswerMatch = cast super.clone();
		a.textRight = this.textRight;
		a.textLeft  = this.textLeft;
		return a ;
	}

	override public function toGift(answers:Answers){
		var s = "=" + Question.unescape(textLeft) + "->" + Question.unescape(textRight) + (feedback!= "" ? "#" + Question.unescape(feedback) : "");
		return s;
	}

	override public function isValid():Bool {
	    if ((textLeft =="") && (textRight =="")) return false;
		return true; 
    }

}