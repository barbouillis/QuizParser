package quizparser;

import quizparser.Answer;
import quizparser.Question.QuestionType;
import quizparser.Answer.AnswerNumerical;
import quizparser.Answer.AnswerType;
import quizparser.Question.QuestionType;

class Answers {

	public var list:Array<Answer> = new Array();
	public var type:Question.QuestionType = null;
	public var generalFeedback : String = "";


	public function new ():Void {
	
	}

	public function isValid() {
		if (type == null) return false;
		switch (type) {
			case QuestionType.Description:
				return true;
			case QuestionType.Essay:
				return true;
			case QuestionType.Match:
				if (list.length == 0) return false;
				var onlyInvalid:Bool = true;
				for (a in list) {
					if (a.isValid()) onlyInvalid = false;
				}
				return !onlyInvalid;
			default:
				if (list.length == 0) return false;
				return true;	
		}

	}

	public function checkAnswer(d:Dynamic) {

		var returnA:Answer = null;

		switch (type) {
			case QuestionType.Numerical:
				var fraction = 0;
				for ( a in list) {
					var n: AnswerNumerical = cast a;
					if ((n.minimumValue<= d) && (n.maximumValue >= d)) {
						if (n.fraction > fraction ) returnA = a;
					}
				}
			default:
			
		}

		return returnA;

	}



	public function toGift():String{
		if (list.length == 0){
			return "{}";
		}
		
		if (list.length == 1){
			return "{"+(list[0].answerType == Numerical ? "#" : "") + list[0].toGift(this) + (generalFeedback != "" ? "####"+generalFeedback:"" )+ "}";
		}
		var s = "{" + (list[0].answerType == Numerical ? "#" : "") +  " \n";
		
		for ( a in list){
			s += "\t" + a.toGift(this) +"\n";
		}
		if (generalFeedback != "") s += "####"+generalFeedback + "\n";
		s += "}";
		return s;
	}

	public function parseAnswers(s:String){

		var re = ~/\s(?=[=~])/g;  // Answer Separator  Unescaped  "=" and "~"


		var reGeneralFeedback = ~/[^\\]####/i;
		


		// We suppose that that the general feedback is at the end , no details in Moodle Specs
		if (reGeneralFeedback.match(s)){
			generalFeedback = reGeneralFeedback.matchedRight();
			s = reGeneralFeedback.matchedLeft();
		}



		var isNumerical = (s.charAt(0) == "#")? true : false;
		if (isNumerical) {
			s = s.substr(1);
			s = StringTools.trim(s);
		}
		for ( a in re.split(s)){
			var ans = Answer.parseAnswer(a, isNumerical);
			
			list.push(ans);
		}
		update();

	}

	public function update(){

		findQuestionType();
	}

	public function findQuestionType(){

		if (list.length == 0) return;

		if (list[0].answerType == AnswerType.TrueFalse){
			type = QuestionType.TrueFalse;
			list = list.slice(0,1);
			completeTrueFalse();
			return ;
		}

/*
		// If there's one multiple answer ( one fraction), it's a multiple answer
		for (a in list){
			if (a.answerType == AnswerType.MultipleAnswers){
				type = QuestionType.MultiAnswers;
				return ;
			}
		}*/

		



		//Match
		for (a in list){
			if (a.answerType == AnswerType.Match){
				type = QuestionType.Match;
				return ;
			}
		}

		//Numerical
		for (a in list){
			if (a.answerType == AnswerType.Numerical){
				type = QuestionType.Numerical;
				return ;
			}
		}

		

		// If all aere all multiple, it's amultiple anwser

		// If there's one multiple answer ( one fraction), it's a multiple answer
		var allMultiples = false;
		for (a in list){
			if ((a.answerType == AnswerType.Multiple))
			{
				//type = QuestionType.MultiAnswers;
				allMultiples = true;
				//return ;
			}
			else {
				allMultiples = false;
				break;
			}
		}
		if (allMultiples) {
			type = QuestionType.MultiAnswers;
			return ;
		}

		// If there's  one answer with = and one with a tilde,  

		// If there 's  one answer with a tilde "~" it's a multiple choice
		for (a in list){
			if (a.answerType == AnswerType.Multiple){
				type = QuestionType.Multichoice;
				return ;
			}
		}

		// Missing 


		// Otherwise if there's a "=" 
		for (a in list){
			if (a.answerType == AnswerType.MultipleOrShort){
				type = QuestionType.Short;
				return ;
			}
		}
	}


	public function completeTrueFalse(){
		var answerA = list[0];
		if (list.length == 1){
			var answerB = new AnswerTrueFalse();
			answerB.text = (answerA.text == "true") ? "false" : "true";
			answerB.fraction =  100 - answerA.fraction;
			list.push(answerB);
		}
	}



	/**
	 * Useful to see if all answers are correct
	 * Useful for missingWords, if all answers are correct, then it is usually dropdown
	 */
	public function areAllCorrect() {
		for (a in list) {
			if (a.fraction != 100) {
				return false;
			}
		}
		return true;
	}

	/*

	Checks à avoir
	- checker si les multi sont bien multis*/


}