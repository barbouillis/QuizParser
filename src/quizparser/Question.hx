package quizparser;


/* The GIFT import filter was designed as an easy to use method
 * for teachers writing questions as a text file. It supports most
 * question types and the missing word format.
 *
 * Multiple Choice / Missing Word
 *     Who's buried in Grant's tomb?{~Grant ~Jefferson =no one}
 *     Grant is {~buried =entombed ~living} in Grant's tomb.
 * True-False:
 *     Grant is buried in Grant's tomb.{FALSE}
 * Short-Answer.
 *     Who's buried in Grant's tomb?{=no one =nobody}
 * Numerical
 *     When was Ulysses S. Grant born?{#1822:5}
 * Matching
 *     Match the following countries with their corresponding
 *     capitals.{=Canada->Ottawa =Italy->Rome =Japan->Tokyo}
 *
 * Comment lines start with a double backslash (//).
 * Optional question names are enclosed in double colon(::).
 * Answer feedback is indicated with hash mark (#).
 * Percentage answer weights immediately follow the tilde (for
 * multiple choice) or equal sign (for short answer and numerical),
 * and are enclosed in percent signs (% %). See docs and examples.txt for more.
 *
 * */

enum QuestionType {
	Description;
	Essay;
	Numerical;
	MissingWords;
	Multichoice;
	MultiAnswers;
	Match;
	Short;
	TrueFalse;
}
/*
enum TextFormat {
	Html;
	Markdown;
	Plain;
	Moodle;
	Custom(s:String);
}*/


class Question {
	/** likely to be valid */ 
	public var state:Bool = true; 	// presomption positive : switch to false
									// when there is proof the question is not good enough ..
	public var id: Null<Int>;

	// My
	public var type: QuestionType;
	public var answers: Answers;
	public var generalFeedback(get, set): String;
	public var title: String = "";
	public var text: String  = "";
	public var textAfter: String = ""; // Text to the right of the answers
	public var qFormat: String;
	public var category: String;
	public var userdata:Dynamic;
	public var tags:Array<String> = new Array();


	// utilities var

	public var nbrAnswers(get, null):Int;
	public var correctAnswer(get, null):String;


	// indications given to the quiz master
	public var allowShuffle: Bool = true;
	public var allowMultipleAnswers: Bool;
	public var allowView: Bool;
	public var allowCase: Bool;



	public function addTag(s:String){
		tags.push(s);
	}

	public function removeTag(s:String){
		tags.remove(s);
	}

	public function get_nbrAnswers(){
		return answers.list.length;
	}

	public function get_correctAnswer(){
		for (a in answers.list){
			if (a.fraction == 100)
				return a.text;
		}

		return null;
	}

	public function get_generalFeedback() {
		return answers.generalFeedback;
	}

	public function set_generalFeedback(feedback:String) {
		answers.generalFeedback = feedback;
		return answers.generalFeedback;
	}
	

	public function isMissingWord(){
		if (textAfter != "") return true;
		return false;
	}

	//public function isThisAnswerCorrect()


	public function toGift():String{
		var s = "";
		if (id != null) s = "// " +"id:" + id ;
		
		for (t in tags){
			if (s == "") s = "//";
			s+=" [tag:"+ t+"]";
		}
		if (s!="") s+= "\n";
		s += "::" + Question.unescapeText(title) + "::" +"\n";
		if (text != "") s += Question.unescapeText(text) +"\n";
		if (type == Description) return s;
		if (answers != null ) s += answers.toGift();
		s += Question.unescapeText(textAfter) +"\n";
		return s;

	}
	
	public function new ():Void {
	}
   /*
	// only interested by the question
	public function quickReadLines(lines:String, questionOnly:Bool=false): Void {
		return readLines(lines, true);
	}*/


	public function checkAnswer(d:Dynamic) {

	}

	public function isValid() {
		if (answers != null ) return answers.isValid();
		return false;
	}
	

	public function parseText(s:String){
		if ((s == null) || s == "")
			return;

		var formatReg = ~/\[(.*)\]/i;

		if (formatReg.match(s)){
			qFormat = formatReg.matched(0);
			s = formatReg.matchedRight();
		}

		var nameReg = ~/::(.*)::/im;
		var hasTitle = nameReg.match(s);
		if (hasTitle){
			this.title = Question.escape(StringTools.trim(nameReg.matched(1)));
			s = nameReg.matchedRight();
		}

		// var textReg:EReg  =  ~/(?<!\\){/im; No negative  behind in old browsers

		var textReg:EReg = ~/[^\\]{/im;

		var answerBegin:EReg = ~/[^\\]{/im;

		if (s.charAt(0) == "{") {
			this.text = Question.escape(StringTools.trim(s.substr(1)));
		}
		else if (textReg.match(s)){
			this.text = Question.escape(StringTools.trim(s.substr(0,textReg.matchedPos().pos+1)));
		}
		else{
			this.text = Question.escape(StringTools.trim(s));
			this.type = QuestionType.Description;
		}
		

		// MOODLE SPECS : If no question name is specified, the entire question will be used as the name by default. 

		if (!hasTitle) this.title = this.text;

		
		var answerEnd:EReg   = ~/[^\\]}/im;

		this.answers = new Answers();
		if (answerBegin.match(s) && answerEnd.match(s)){
			var answersS = StringTools.trim(s.substring(answerBegin.matchedPos().pos + 2 , answerEnd.matchedPos().pos + 1 ));
			//var answersS = StringTools.trim(answerRe.matched(1));
			if (answersS == ""){
				this.type = QuestionType.Essay;
				return;
			}
			this.textAfter = s.substring(answerEnd.matchedPos().pos +3);

			answers.parseAnswers(answersS);
			if (this.textAfter != ""){
				this.type = MissingWords;
			}
			else  {
				this.type = answers.type;
			}
			
			
		}		

	}

	// Utilities Functions

	// Escape special Characters ~ = # { }
	// Which answer equals 5? {
    // ~ \= 2 + 2
    // = \= 2 + 3
    // ~ \= 2 + 4
	public static function escape(s:String):String{
		var escRe=~/\\([~=#\{\}])/g;
		return escRe.replace(s,"$1");
	}

	public static function unescape(s:String):String{
		if (s== null) return "";
		var escRe=~/([~=#\{\}])/g;
		return escRe.replace(s,"\\$1");
	}

	public static function unescapeText(s:String):String{
		var escRe=~/([#\{\}])/g;
		return escRe.replace(s,"\\$1");
	}


}
