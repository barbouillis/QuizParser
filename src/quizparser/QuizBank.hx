package quizparser;

/*
is able to read the meta of the question (allowView, allowMultipleAnswers, allowShuffle, etc ...) 
and delivers the contents of the selected questions (label, name, format and the answers (object ..)) using the method 
revealByAnswers of the class Question
retains the origin of the question : String or File // no
and gives the Url of the media used in both the question and the answers / /no
it can discard the question in "bad state"; 


//TODO tags
a line begining by ; or / is seen as seen as a development comment and not read
by Quiz
* quid des "ressources" media image,son demandés par la question ou les réponses ...
* nécessite une lecture et une interprétation .. on laisse au renderer ..
*/
class QuizBank {


	public var questions:Array<Question> = new Array();
	public var questionsMap : Map<Int, Question> = new Map();

	inline public static var  UNTAGGED = "UNTAGGED";




	public function new():Void {

	}

	public function toGift() {
		var s = "";
		for ( q in questions) {
			s += q.toGift() + "\n";
		}
		return s;
	}

	public function addQuestion(q:Question, ?id:Int):Void{
		questions.push(q);
		// Check if question has fixed id
		if (id != null){
			// If there's already a question with this id. Change old question id.
			if (!questionsMap.exists(id)){
				questionsMap.set(id,q);
			}
			else{
				var oldQ = questionsMap.get(id);
				//if (oldQ.id == null)  trace("adding question : conflicting id" + oldQ.id);
				questionsMap.set(getFirstEmptyId(), oldQ);
				questionsMap.set(id,q);
			}
		}
		else{
			questionsMap.set(getFirstEmptyId(),q);
		}
	}


	private function getFirstEmptyId():Int{
		var qL = questions.length;
		var i = 0;
		while (i<qL){
			if (!questionsMap.exists(i)) break;
			i++;
		}
		return i;

	}

	public function removeQuestion(q:Question):Void{
		questions.remove(q);
		for ( k => v in questionsMap){
			if ( v == q) questionsMap.remove(k);
		}
		
	}

	public function allTags():Array<String> {
		var tags:Array<String> = [];
		for (q in questions){
			for (t in q.tags) {
				if (!tags.contains(t)) tags.push(t);
			}
		}
		return tags;

	}

	public function extractTaggedQuestions():Map<String, Array<Question>> {
		var map:Map<String, Array<Question>> = new Map();
		for (q in questions) {
			if (!q.isValid()) continue;
			var isTagged = false;
			for (t in q.tags) {
				if (!map.exists(t))  map[t] = [];
				map[t].push(q);
				isTagged = true;
			}
			if (!isTagged) {
				if (!map.exists(UNTAGGED))  map[UNTAGGED] = [];
				map[UNTAGGED].push(q);
			}
			
		}
		return map;
	}

/*
	public function allCategories():Array<String> {
		var tags:Array<String> = [];
		for (q in questions){
			for (t in q.tags) {
				if (!tags.contains(t)) tags.push(t);
			}
		}
		return tags;

	}*/

	public function extractCategoriesToQuizBank(s:String):QuizBank{
		var qb = new QuizBank();
		for (q in questions){
			if (q.category == s){
				qb.addQuestion(q);
			}
		}
		return qb;
	}

	public function getQuestionWithTitle(s:String):Question{
		var title = StringTools.trim(s);
		for (q in questions){
			if (q.title == title ){
				return q;
			}
		}
		return null;
	}

	public function getQuestionWithId(id:Int):Question{
		return questionsMap.get(id);
	}

	public function getRandomQuestion():Question{

		var high = questions.length;

		var r = Math.floor(Math.random() * high);
		return questions[r];
	}

	public function getAllQuestions():Array<Question>{
		return questions;
	}

	/*
	public function joinQuizBank(qb:QuizBank):QuizBank{
	}
	public function clone

	*/


	public function concat(qb:QuizBank) {
		this.questions = this.questions.concat(qb.questions);
		for ( q in qb.questions) {
			this.addQuestion(q);
		}

		return this;
	}



	public function loadBankFromString (data:String, ?number:Int, ?type: String, ?random: Bool) {
	 // the ? are not implemented !
	
		var questions: Array<Question> = [];
		var splitter = ~/\r\n|\n\r|\n|\r/gm; //EOF for any system
		var lines:Array<String> = splitter.split(data); // remove trailing/leading whitespaces and collapses sequences of whitespaces in one whitespace
		var q = new Question();
		var _arrayContent:Array<String> = []; 
		var content = "";
		var i = 0;

/*
		while (i< 0){
			splitter.matchSub(data, i);
			splitter.match

		}*/



//  [id:123] [tag:basic] [tag:set 1]


		// Unefficient to cut lines and then join them
		var cat:String = "";
		var id  = 0;
		for (line in lines) {
			if (StringTools.startsWith(line,"//")){
				// Comments [id:123] [tag:basic] [tag:set 1]
				id = parseCommentsForQuestion(line,q);

			}
			else if ( (line.charAt(0) == ";") || (line.charAt(0) == "/") ){
				continue;
			}
			// Metadata
			else if (line.charAt(0) == "!"){
				if (line.indexOf("dM") > 0)
					q.allowMultipleAnswers = false;
				if (line.indexOf("dS") > 0)
					q.allowShuffle = false;
				if (line.indexOf("dV") > 0)
					q.allowView = false;
				if (line.indexOf("dC") > 0)
					q.allowCase = false;
			}
			// Moodle Metadata
			else if (line.charAt(0) == "$"){
				if (StringTools.startsWith(line,"$CATEGORY: " )){
					cat = line.substr(11);
				}
			}
			else if (StringTools.trim(line) != "" ){
				_arrayContent.push(line);

			}
			else { 
				// Quiz questions are separated by empty lines
				if (_arrayContent.length > 0){
					q.parseText(_arrayContent.join(" "));
					//q.readLines(_arrayContent.join(" "),false); // false  as not only interested in question 
					//q.id = i;	 // starting with 0
					addQuestion(q,id);
					_arrayContent = [];	//get ready for another item
					i += 1;
					q = new Question();
					q.category = cat;
				}
			}
		}

		return this;
	}

	public function parseCommentsForQuestion(s:String, q:Question):Null<Int>{
		var coReg = ~/\[([^\]\[]+):([^\]\[]+)]/i;
		var i = 0;
		var id = null;

		while (coReg.matchSub(s,i)){
			i = coReg.matchedPos().pos + coReg.matchedPos().len;
			switch(StringTools.trim(coReg.matched(1))){
				case "tag": q.addTag(StringTools.trim(coReg.matched(2)));
				case "id" : id = Std.parseInt(StringTools.trim(coReg.matched(2)));
			}
		}

		return id;
	}

}