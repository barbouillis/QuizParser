package quizparser;

import quizparser.Answer;
import quizparser.Answer;
import quizparser.Question;
import quizparser.Question;
import quizparser.QuizBank;
import quizparser.QuizBank;

class QuizBankPlayer {

    public var shuffle:Bool =false;

    public var questions:Array<Question> = new Array();

    public var questionAttempts:Array<QuestionAttempt> = new Array();

    public var pos:Int = 0;

    public var qb:QuizBank;
    public function new(qb:QuizBank) {
        this.qb = qb;

    }

    public function init() {
        if (shuffle) doShuffle();
        else {
            questions = qb.questions.copy();
        }
    }

    public function doShuffle() {
        this.questions = [];
        var questions = qb.questions.copy();
        while (questions.length > 0) {
            var i = Std.int(Math.random() * questions.length);
            this.questions.push(questions.splice(i, 1)[0]);
        }
    }


    public function getCurrentScore(){
        var score = 0.0;
        for (q in questionAttempts) {
            score +=q.score;
        }
        return score;
    }

    public function getMaxPartialScore() {

    }

    public function getMaxScore() {

    }

    public function getActualQuestionAttempt() {
        if (questionAttempts[pos] == null) {
            if (questions[pos] == null) return null;
            questionAttempts[pos] = new QuestionAttempt(questions[pos]);
        }
        return questionAttempts[pos];
    }

    public function goToNextQuestion() {
        pos++;
    }

    public function goToPreviousQuestion() {
        pos--;
    }

    public inline function isLastQuestion() {

        return (pos == (questions.length -1 ));
    }

    public inline function isFirstQuestion() {

        return (pos == 0);
    }

    public function attemptForQuestion(q:Question) {
        return questionAttempts[questions.indexOf(q)];
    }

    
}

class QuestionAttempt {

    public var question:Question;

    public var answers:Array<{answered:Dynamic, answer:Answer}> = [];

    public var score(get, null):Float;

    public var isValidated:Bool;

    var selectedAnswer:Dynamic;
    public function new(q:Question) {
        this.question = q;
    }

    public function setAnswer(a:Answer, answered:Dynamic) {
        var found = false;
        for (ans in answers) {
            if ( ans.answer == a  ) {
                found = true;
                ans.answered = answered;
            }
        }
        if (!found) {
            answers.push({answered: answered, answer:a });
        }
    }

    public function addAnswer(a:Answer) {
       // answers.push({answered: null, answer:a });
    }

    public function get_score() {
        var score = 0.0;
        for (a in answers) {
            if (a.answer !=null) {
                score += a.answer.fraction;
                trace(a.answer.fraction);
            }
        }

        return score;
    }


}